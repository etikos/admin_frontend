<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="assets/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <title>Etikos</title>
    <style>
        .header {

            height: 95vh;
            background-image: url('assets/img/landing-page.png');
            display: flex;
            flex-direction: column;
            align-items: center;
            background-size: 100% 100%;
            justify-content: center;
            margin-bottom: 30px;
        }

        .tombol-landing {
            position: absolute;
            animation: mymove 8s;
            left: 5%;
            /*sama*/
            top: 110px;
        }

        @keyframes mymove {
            0% {
                top: 0px;
            }

            40% {
                /*sama*/
                top: 110px;
            }

            100% {
                down: 0px;
            }
        }
    </style>
</head>

<body>
    <div class="header">
        <div class="container">
            <div class="card-body">
                <div class="tombol-landing">
                    <div class="row">
                        <h1 style="font-size: 4em;">e- Voting Ketua Osis Smada</h1>
                    </div>
                    <div class="row mb-3">
                        <h7 style="font-size: 2em;">Website Pemilihan Online Ketua Osis SMA 2 Paingan</h7>
                    </div>
                    <div class="row ml-5">
                        <button class="btn btn-light mr-4" style="box-shadow: 5px 5px #888888;"> Login Sebagai Admin</button>
                        <button class="btn btn-primary" style="box-shadow: 5px 5px #888888;">Login Sebagai Siswa</button>
                    </div>
                </div>
            </div>
            <div class="fixed-bottom d-flex justify-content-end">
                <a href="#" class="btn"><i class="fab fa-whatsapp fa-3x"></i></a>&nbsp;
                <a href="#" class="btn"><i class="fab fa-facebook fa-3x"></i></a>&nbsp;
                <a href="#" class="btn"><i class="fab fa-twitter fa-3x"></i></a>&nbsp;
                <a href="#" class="btn"><i class="fab fa-instagram fa-3x"></i></a>&nbsp;
            </div>
        </div>
    </div>
</body>

</html>