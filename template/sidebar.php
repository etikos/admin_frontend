<!-- Sidebar -->
<ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: #152349;">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../admin/beranda.php" style="background-color: #40A2D6;">
        <div class="sidebar-brand-icon">
            <!-- <i class="fas fa-laugh-wink"></i> -->
            <img src="../assets/img/logo.png" width="50" height="50" />
        </div>
        <div class="sidebar-brand-text mx-2" style="font-size: 1.5em; color: white;">
            ETIKOS
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="../admin/beranda.php">
            <!-- <i class="fas fa-fw fa-home fa-3x" style="font-size: 2em; color: white;"></i> -->
            <img src="../assets/img/icon-home.png" width="30" height="30" style="padding-bottom: 2px;">
            <span style="font-size: 20px; color: white;">Beranda</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <!-- <div class="sidebar-heading">
        Interface
    </div> -->

    <!-- Nav Item - Pages Collapse Menu -->
    <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Components</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Custom Components:</h6>
                <a class="collapse-item" href="buttons.html">Buttons</a>
                <a class="collapse-item" href="cards.html">Cards</a>
            </div>
        </div>
    </li> -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="../admin/siswa.php">
            <!-- <i class="fas fa-fw fa-user-graduate" style="font-size: 2em; color: white;"></i> -->
            <img src="../assets/img/icon-siswa.png" width="30" height="30" style="padding-bottom: 2px;">
            <span style="font-size: 20px; color: white;">Siswa</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <!-- <div class="sidebar-heading">
        Addons
    </div> -->

    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="../admin/calon.php">
            <!-- <i class="fas fa-fw fa-person-booth" style="font-size: 2em; color: white;"></i> -->
            <img src="../assets/img/Group 11.png" width="30" height="30" style="padding-bottom: 2px;">
            <!-- <div class="cocok"> -->
            <span style="font-size:20px;color: white;">Kandidat</span>
            <!-- <span style="color: white;">Kandidat</span> -->
            <!-- </div> -->

        </a>
    </li>

</ul>
<!-- End of Sidebar -->