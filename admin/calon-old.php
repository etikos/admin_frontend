<?php
include('../template/header.php');
include('../template/sidebar.php');
include('../template/topbar.php');
?>




<!-- Begin Page Content -->
<div class="container-fluid">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" style="background: rgba(255, 255, 255, 1); border: 0px solid rgba(245, 245, 245, 1); border-radius: 4px; display: block;">
            <li class="breadcrumb-item active" aria-current="page">Kandidat</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card shadow mb-4">
                <!-- <div class="card-header py-3">
                    <h3 class="m-0 font-weight-bold">Tabel Calon Ketua OSIS</h3>
                </div> -->
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-lg-4">
                            <h3 class="m-0 font-weight-bold">Tabel Calon Ketua OSIS</h3>
                        </div>
                    </div>
                    <hr style="border: 3px solid #C4C4C4;">
                    <div class="row mb-2">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option value="">-Aksi Massal-</option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                    <option value="">4</option>
                                    <option value="">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <a href="#" class="btn btn-info shadow"><i class="fas fa-database"></i></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-striped table-responsive">
                                <thead>
                                    <tr>
                                        <th scope="col"><i class="fas fa-check-square"></i></th>
                                        <th scope="col">First</th>
                                        <th scope="col">Last</th>
                                        <th scope="col">Last</th>
                                        <th scope="col">Handle</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                            </div>
                                        </th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                        <td>
                                            <a href="#"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="#"><i class="fas fa-trash"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger">suspend</a>
                                            <a href="#"><i class="fas fa-ban"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                            </div>
                                        </th>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                        <td>@mdo</td>
                                        <td>
                                            <a href="#"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="#"><i class="fas fa-trash"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger">suspend</a>
                                            <a href="#"><i class="fas fa-ban"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                            </div>
                                        </th>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>@mdo</td>
                                        <td>
                                            <a href="#"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="#"><i class="fas fa-trash"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger">suspend</a>
                                            <a href="#"><i class="fas fa-ban"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php include('../template/footer.php'); ?>