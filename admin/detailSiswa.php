<?php
include('../template/header.php');
include('../template/sidebar.php');
include('../template/topbar.php');
?>




<!-- Begin Page Content -->
<div class="container-fluid">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" style="background-color: white;">
            <li class="breadcrumb-item"><a href="#">Siswa</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail Siswa</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <h3 class="m-0 font-weight-bold">Detail Data Siswa</h3>
                        </div>
                    </div>
                    <hr style="border: 3px solid #C4C4C4;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Nama</label>
                                <label for="name" class="col-sm-1 col-form-label">:</label>
                                <label for="name" class="col-sm-8 col-form-label">George Rudy</label>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">NIS</label>
                                <label for="name" class="col-sm-1 col-form-label">:</label>
                                <label for="name" class="col-sm-8 col-form-label">111111</label>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                <label for="name" class="col-sm-1 col-form-label">:</label>
                                <label for="name" class="col-sm-8 col-form-label">12-12-2001</label>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Kelas</label>
                                <label for="name" class="col-sm-1 col-form-label">:</label>
                                <label for="name" class="col-sm-8 col-form-label">12 A 1</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php include('../template/footer.php'); ?>