<?php
include('../template/header.php');
include('../template/sidebar.php');
include('../template/topbar.php');
?>




<!-- Begin Page Content -->
<div class="container-fluid">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" style="background-color: white;">
            <li class="breadcrumb-item"><a href="#">Kandidat</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tambah Calon</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card shadow mb-4">
                <!-- <div class="card-header py-3">
                    <h3 class="m-0 font-weight-bold">Tabel Siswa</h3>
                </div> -->
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <h3 class="m-0 font-weight-bold">Tambah Data Calon</h3>
                        </div>
                    </div>
                    <hr style="border: 3px solid #C4C4C4;">
                    <div class="row">
                        <div class="col-sm-12">
                            <form>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Nama</label>
                                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama Calon">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">No Urut</label>
                                            <select class="form-control" id="exampleFormControlSelect1">
                                                <option>--Pilih No Urut--</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Nama Wakil</label>
                                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama Wakil">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Upload Foto Calon dan Wakil</label>
                                            <div class="image-upload">
                                                <label for="file-input">
                                                    <img id="previewImg" src="../assets/img/icon-upload.png" class="img-thumbnail" />
                                                </label>
                                                <input id="file-input" type="file" name="foto_calon" onchange="previewFile(this);" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Visi</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Masukkan Visi Calon"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class=" form-group">
                                            <label for="exampleFormControlTextarea1">Misi</label>
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Masukkan Misi Calon"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary mb-2">Tambah</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php include('../template/footer.php'); ?>