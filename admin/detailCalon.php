<?php
include('../template/header.php');
include('../template/sidebar.php');
include('../template/topbar.php');
?>




<!-- Begin Page Content -->
<div class="container-fluid">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" style="background-color: white;">
            <li class="breadcrumb-item"><a href="#">Siswa</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail Kandidat</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <h3 class="m-0 font-weight-bold">Detail Data Kandidat</h3>
                        </div>
                    </div>
                    <hr style="border: 3px solid #C4C4C4;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row d-flex justify-content-center">
                                <div class="col-sm-8">
                                    <img src="../assets/img/nurhadi_aldo.png" class="img-thumbnail" alt="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Nama Calon</label>
                                <label for="name" class="col-sm-1 col-form-label">:</label>
                                <label for="name" class="col-sm-7 col-form-label">Nurhadi</label>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Nama Wakil</label>
                                <label for="name" class="col-sm-1 col-form-label">:</label>
                                <label for="name" class="col-sm-7 col-form-label">Adil</label>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">No Urut</label>
                                <label for="name" class="col-sm-1 col-form-label">:</label>
                                <label for="name" class="col-sm-7 col-form-label">01</label>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Visi</label>
                                <label for="name" class="col-sm-1 col-form-label">:</label>
                                <label for="name" class="col-sm-7 col-form-label">Bernyanyi Bernyanyi</label>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Misi</label>
                                <label for="name" class="col-sm-1 col-form-label">:</label>
                                <textarea for="name" class="col-sm-7 form-control" rows="3" readonly>Bernyanyi Bernyanyi</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php include('../template/footer.php'); ?>