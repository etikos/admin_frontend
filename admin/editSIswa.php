<?php
include('../template/header.php');
include('../template/sidebar.php');
include('../template/topbar.php');
?>




<!-- Begin Page Content -->
<div class="container-fluid">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" style="background-color: white;">
            <li class="breadcrumb-item"><a href="#">Siswa</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Siswa</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <h3 class="m-0 font-weight-bold">Edit Data Siswa</h3>
                        </div>
                    </div>
                    <hr style="border: 3px solid #C4C4C4;">
                    <div class="row">
                        <div class="col-sm-12">
                            <form>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">NIS</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" value="12345678">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Nama</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" value="Grego Bara">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Tanggal Lahir</label>
                                    <input type="date" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com" value="2013-01-08">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Kelas</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>--Pilih Kelas--</option>
                                        <option>10A1</option>
                                        <option>10I1</option>
                                        <option>11A1</option>
                                        <option>11I1</option>
                                        <option selected>12A1</option>
                                        <option>12I1</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mb-2">Edit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php include('../template/footer.php'); ?>