<?php
include('../template/header.php');
include('../template/sidebar.php');
include('../template/topbar.php');
?>




<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-12 d-flex justify-content-center">
                            <h3 class="m-0 font-weight-bold">Ubah Password Admin</h3>
                        </div>
                    </div>
                    <hr style="border: 3px solid #C4C4C4;">
                    <div class="row">
                        <div class="col-sm-12">
                            <form>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Password Lama</label>
                                    <input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Password Lama Anda">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Password Baru</label>
                                    <input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Password Baru Anda">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Konfirmasi Password Baru</label>
                                    <input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Ulangi Password Baru Anda">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mb-2">Edit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php include('../template/footer.php'); ?>