<?php
include('../template/header.php');
include('../template/sidebar.php');
include('../template/topbar.php');
?>




<!-- Begin Page Content -->
<div class="container-fluid">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb" style="background: rgba(255, 255, 255, 1); border: 0px solid rgba(245, 245, 245, 1); border-radius: 4px; display: block;">
            <li class="breadcrumb-item active" aria-current="page">Siswa</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <h3 class="m-0 d-flex justify-content-center font-weight-bold">Tabel Siswa</h3>
                        </div>
                    </div>
                    <hr style="border: 3px solid #C4C4C4;">
                    <div class="row mb-2">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option value="">-Aksi Massal-</option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                    <option value="">4</option>
                                    <option value="">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <a href="#" class="btn btn-light border border-primary text-primary mb-3"><i class="fas fa-plus"></i>&nbsp;Siswa</a>
                            <a href="#" class="btn btn-light border border-primary text-primary mb-3"><img src="../assets/img/icon-xlsx.png" width="20px" height="20px" alt="">&nbsp;Upload</a>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option value="">-Filter-</option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                    <option value="">4</option>
                                    <option value="">5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive mr-4 ml-4">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th><i class="fas fa-check-square"></i></th>
                                        <th>NIS</th>
                                        <th>Nama</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Kelas</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th><i class="fas fa-check-square"></i></th>
                                        <th>NIS</th>
                                        <th>Nama</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Kelas</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                            </div>
                                        </td>
                                        <td>000001</td>
                                        <td>Leomord</td>
                                        <td>04-12-2005</td>
                                        <td>11A1</td>
                                        <td>
                                            <a href="#" class="mb-1 mr-3"><i class="fas fa-eye"></i></a>
                                            <a href="#" class="mb-1 mr-3"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="mb-1 mr-3"><i class="fas fa-trash"></i></a>
                                            <a href="#" class="mb-1 mr-3 btn btn-sm btn-light text-primary border border-primary">reset password</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                            </div>
                                        </td>
                                        <td>000002</td>
                                        <td>Yu Zhong</td>
                                        <td>04-13-2005</td>
                                        <td>11A1</td>
                                        <td>
                                            <a href="#" class="mb-1 mr-3"><i class="fas fa-eye"></i></a>
                                            <a href="#" class="mb-1 mr-3"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="mb-1 mr-3"><i class="fas fa-trash"></i></a>
                                            <a href="#" class="mb-1 mr-3 btn btn-sm btn-light text-primary border border-primary">reset password</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<?php include('../template/footer.php'); ?>