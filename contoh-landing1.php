<!DOCTYPE html>
<html>

<head>
    <title>FullScreenLandingPage</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" type="text/css" href="index.css"> -->
    <style>
        * {
            padding: 0px;
            margin: 0px;
            font-family: monospace;
        }

        header {

            height: 100vh;
            /* background-image: url('https://images.unsplash.com/photo-1529420705456-5c7e04dd043d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f23d7c077ab1f917d774c638d0aefadc&auto=format&fit=crop&w=700&q=80'); */
            background-image: url('assets/img/landing-page.png');
            display: flex;
            flex-direction: column;
            align-items: center;
            /* background-size: cover; */
            background-size: 100% 100%;
            justify-content: center;
            margin-bottom: 30px;
        }

        .main {
            width: 90%;
            max-width: 100%;
            margin: 10px 5% 0 5%;
            font-size: 1.4em;
            line-height: 1.4em;
            /*padding: 15px;*/
            background-color: #eee;
            height: 100vh;
        }

        .main p {
            padding: 10px;
        }

        #down {
            position: absolute;
            top: 85vh;
            size: 40px;
            padding: 20px;
            font-size: 2.5em;
        }

        #down:hover {
            cursor: pointer;
            background-color: #eee;
            opacity: 0.8;
        }
    </style>
</head>

<body>

    <header>
        <h1>Welcome to my LandingPage</h1>
        <p>How are you doing?</p>
        <span id="down">&#8595;</span>
    </header>

    <div class="main">
        <h3 style="text-align: center;">Some Sample Content</h3>
        <p>
            Web browsers receive HTML documents from a web server or from local storage and render the documents into multimedia web pages. HTML describes the structure of a web page semantically and originally included cues for the appearance of the document.
        </p>
        <p>
            Web browsers receive HTML documents from a web server or from local storage and render the documents into multimedia web pages. HTML describes the structure of a web page semantically and originally included cues for the appearance of the document.
        </p>
        <p>
            Web browsers receive HTML documents from a web server or from local storage and render the documents into multimedia web pages. HTML describes the structure of a web page semantically and originally included cues for the appearance of the document.
        </p>
        <p>
            Web browsers receive HTML documents from a web server or from local storage and render the documents into multimedia web pages. HTML describes the structure of a web page semantically and originally included cues for the appearance of the document.
        </p>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = function() {
            window.scrollTo(0, 0);
        }

        $(document).ready(function() {
            $('#down').on('click', function() {
                $("html").scrollTop(0);
                console.log("Clicked!");
                $('html, body').animate({
                    scrollTop: $(".main").offset().top
                }, 1000);
            });
        });
    </script>
</body>

</html>